<?php 
  /**
   * Service Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "Services";
  // adding main head file
  require __DIR__.'/../inc/head.inc.php'; 
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <section>
      <div id="container"><!-- container div started -->
        <div id="inner"><!-- inner div started -->
          <h1><span class="underline">Services</span></h1>
          <!-- services div started -->
          <div id="servies_info">
            <div class="box">
              <img src="Images/about_pic.jpg" alt="photo" />
              <div>
                <span class="text">Pre-Wedding Shoot</span>
              </div>
            </div>
            <div class="box">
              <img src="Images/s2.jpg" alt="photo" />
              <div>
                <span class="text">Wedding Shoot</span>
              </div>
            </div>
            <div class="box">
              <img src="Images/s3.jpg" alt="photo" />
              <div>
                <span class="text">Portfolio</span>
              </div>
            </div>
            <div class="box">
              <img src="Images/s4.jpg" alt="photo" />
              <div>
                <span class="text">Baby Shoot</span>
              </div>
            </div>
            <div class="box">
              <img src="Images/s5.jpg" alt="photo" />
              <div>
                <span class="text">Family Shoot</span>
              </div>
            </div>
            <div class="box">
              <img src="Images/s6.jpg" alt="photo" />
              <div>
                <span class="text">Post-Wedding Shoot</span>
              </div>
            </div>
          </div><!-- services div ending -->
        </div><!-- inner div ending -->
      </div><!-- container div ending -->
    </section>
    
    <!-- Footer PHP File -->
    <?php 
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>   
  </body>
</html>