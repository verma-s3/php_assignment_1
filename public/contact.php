<?php 
  /**
   * Contact Page 
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
  $title = "Contact";
  // Adding man head file
  require __DIR__.'/../inc/head.inc.php'; 
?>
  
  <body>
   
    <!-- header PHP file -->
    <?php
    // adding header file
    require __DIR__.'/../inc/header.inc.php'; 
    ?>
    
    <div id="container"><!-- container div started -->
      <div id="inner"><!-- inner div started -->
        <div id="contact_info"><!-- content_info div starting -->
          <div>
            <h1><span class="underline">Contact us:</span></h1>
            <div class="two_col"><!-- two_col starting -->
              <h2>Address</h2>
              <address>
                Amritsar Road,<br />
                Near Khanna Hospital,<br />
                Moga,Punjab,India,<br />
                142001. 
              </address>
              <h2>Timings:</h2>
              <p>10.00 AM - 8.00 PM</p>
              <h2>Call on:</h2>
              <p>97812-40006, 97816-04346</p>
              <h2>E-mail:</h2>
              <p>kheracolorlabmoga@gmail.com</p>
            </div><!-- two_col ending -->
            <img src="Images/card.jpg" alt="card" />
          </div>
        </div><!-- content_info div ending -->
        <hr />
        <br />
        <div style="margin-top:30px;margin-bottom: 50px;"><!-- table div starting -->
          <table><!-- table starting -->
            <caption>Package information</caption>
            <tr >
              <th colspan="4" style="background: #fcf;">PACKAGE PRICES</th>
            </tr>
            <tr>
              <th>Package 1</th>
              <th>Package 2</th>
              <th>Package 3</th>
              <th>Package 4</th>
            </tr>
            <tr>
              <td>$150</td>
              <td>$250</td>
              <td>$350</td>
              <td>$450</td>
            </tr>
            <tr>
              <td>1 Person</td>
              <td>1-3 Person(s)</td>
              <td>1-2 Person(s)</td>
              <td>1-6 Person(s)</td>
            </tr>
            <tr>
              <td>15 Mintute Session</td>
              <td>20 Mintute Session</td>
              <td>30 Mintute Session</td>
              <td>45 Mintute Session</td>
            </tr>
            <tr>
              <td>10 Digital Images</td>
              <td>20 Digital Images</td>
              <td>30 Digital Images</td>
              <td>40 Digital Images</td>
            </tr>
            <tr>
              <td>Photo release</td>
              <td>Photo release + Facbook Cover</td>
              <td>Photo release</td>
              <td>Photo release + Facebook Cover</td>
            </tr>
          </table><!-- table ending -->
        </div><!-- table div ending -->
        <hr />
        <br />
        <div><!-- div for form started -->
          <h1 style="padding-left: 30px;">Contact Form:</h1>
          <p style="padding-left: 30px;">Please fill the form to chat with us:</p>
          <form id="first_form"
                name="first_form"
                method="post"
                action="http://www.scott-media.com/test/form_display.php"
                autocomplete="on" 
                > <!-- starting of request form -->
            <fieldset>
              <legend>Contact Information</legend>
              <p>All the (<span id="compulsory">*</span>) fields are mandatory</p>
              <p>
                <label for="First_name" class="field">First Name:</label> 
                  <input type="text" 
                         name="First_name" 
                         id="First_name" 
                         maxlength="40"  
                         placeholder="Type Your First name"
                         required="required" /><!-- First name field -->
                </p>

              <p>
                <label for="Last_name" class="field"> Last Name: </label> 
                  <input type="text" 
                         name="Last_name" 
                         id="Last_name" 
                         maxlength="40"
                         placeholder="Type Your Last name" 
                         required="required"/><!-- Last name field -->
              </p>
              
              <p>
              <label for="email_address" class="field">Email: </label>
                <input type="email" 
                       name="email_address" 
                       id="email_address" 
                       placeholder="e.g: soniaverma@gmail.com" 
                       required="required"/><!-- email input field -->
              </p>
              
              <p>
              <label for="comments">Comments: </label>
                <textarea id="comments"
                          name= "comments"
                          rows="4"
                          cols="50"
                          maxlength="50">
                </textarea><!-- text area field -->
              </p>
              
              <p>
                <label for="province">Select your Province:</label>
                  <input list="province_name"  id="province" name="province"/>
                  <datalist id="province_name">
                    <option value="Alberta">
                    <option value="British Coloumbia">
                    <option value="Manitoba">
                    <option value="Saskatchewan">
                    <option value="Toronto">
                    <option value="Quebec">
                  </datalist>
              </p>
              
              <p ><!-- action performed by submit and resrt button -->
                <input type="submit" value="Submit Application" /> &nbsp;  
                <input type="reset" value="Clear Application" /> &nbsp;
              </p>
              
            </fieldset>
          </form>
        </div><!-- div for form ending -->
      </div><!-- inner div ending -->
    </div><!-- container div ending -->
    
    <!-- Footer PHP File -->
    <?php
    // adding footer file 
    require __DIR__.'/../inc/footer.inc.php'; 
    ?>  
  </body>
</html>