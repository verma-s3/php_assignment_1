<?php
/**
   * Configration File
   * last_update: 2019-08-02
   * Created by: Sonia Verma, verma-s3@webmail.uwinnipeg.ca
   * Site name : Khera Digital Studio and Color Lab
   */
// init_set --> Sets the value of a configuration option
ini_set("display_errors",1);
ini_set('error_reporting',E_ALL);

// set sitename Credentials
define("site_name","Khera Color Lab and Digital Studio");

// set Database Credentials

define('DB_DSN', 'mysql:host=localhost;dbname=photostudio');
define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');

?>